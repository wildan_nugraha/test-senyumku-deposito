import {message} from 'antd'

export const tooltipMessage = (value) => {
    message.loading({ content: 'Loading...',key:'updatable'});
    setTimeout(() => {
      message.success({ content: value ,key:'updatable', duration: 2 });
    }, 1000);
  };