export const MIN_16_DIGIT_REGEX = /^(?=.{16,})/;
export const MIN_12_DIGIT_REGEX = /^(?=.{12,})/;
export const ONLY_NUMERICAL_REGEX = /[^0-9]/g
export const EMAIL_FORMAT_REGEX = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i ;
export const PASSWORD_FORMAT_REGEX = /^(?=.*[a-z])+(?=.*[A-Z])+(?=.*[0-9])+(?=.{8,})/
