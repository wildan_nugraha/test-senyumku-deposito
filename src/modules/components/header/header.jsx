import React from "react";
import "./header.scss";
import Logo from "../../../Assets/images/senyumku-logo.svg";
import close from "../../../Assets/images/close.svg";
import Button from "../button/button";
import { useHistory, useLocation } from "react-router-dom";


const Header = () => {
  
  const history = useHistory();
  const location = useLocation()

  const handleClose = () => {
    history.push({ pathname: "/" });
    localStorage.clear()
  }

  return (
    <div className="header">
      <div className="header_container">
        <img src={Logo} alt="logo" className="header_logo" />
        
        <div className="header_container-button">
          {location.pathname === "/"  && (
            <Button name="Daftar" action={()=>history.push({ pathname: "/daftar" })} />
          )}
          {location.pathname === "/" && (
            <Button name="Masuk" action={()=>history.push({ pathname: "/daftar" })} />
          )}
          {location.pathname !== "/"  && (
            <button className="btn-close" onClick={handleClose}>
              <img src={close} alt="" />
            </button>
          )}

        </div>
      </div>
    </div>
  );
};

export default Header;
