import React from "react";
import "./Button.scss"

const Button = ({ name, action }) => {
  
  return (
    <button
      className={name !== "Masuk" ? "button-green" : "button-white"}
        onClick={action}>
      {name}
    </button>
  );
};

export default Button;
