import Button from './button'
import '@testing-library/jest-dom'
import {render,screen} from '@testing-library/react'

describe('Button component test', () => {
   it("Button recieved props", ()=>{
    render(<Button name="Daftar"/>)
    const mockElement = screen.getByText("Daftar")
    expect(mockElement).toBeInTheDocument()
   })
});
