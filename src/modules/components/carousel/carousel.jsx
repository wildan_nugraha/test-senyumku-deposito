import React from "react";
import { Splide, SplideSlide } from "@splidejs/react-splide";

const Carousel = ({data}) => {

  const OPTIONS={ 
    rewind: true,
    width : 200,
    gap   : '10px',
    pagination : false,
    drag : true,
   }
   
  return (
    <Splide options={OPTIONS} >
      {data.map((item, idx) => (
        <SplideSlide key={idx}>
          <div className="card_produk card-2">
            <div className="card_produk-header">{item.title}</div>
            <div className="card_produk-body">
              <div className="main-text">
                {item.text1}
              </div>
              <div className="card_produk-text blue">{item.text2}</div>
              <div className="card_produk-text">
                {item.text3}
              </div>
            </div>
          </div>
        </SplideSlide>
      ))}
    </Splide>
  );
};

export default Carousel;
