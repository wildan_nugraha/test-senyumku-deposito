import React from "react";
import { Steps} from "antd";
import "./steps.scss";
import { REGISTRATION_STEPS } from "../../../Pages/Registration/modules/constants/registration-step.const";

const { Step } = Steps;

const StepComponent = ({stepNum}) => {
  
  return (
    <div className="steps">
      <Steps
        current={stepNum}
        direction="horizontal"
        responsive={false}
        size="small"
        labelPlacement="vertical"
      >
        {REGISTRATION_STEPS.map((item) => (
          <Step key={item.title} title={item.title} />
        ))}
      </Steps>

      
    </div>
  );
};

export default StepComponent;
