import React from "react";
import "./footer.scss";
import { FOOTER_DATA_LIST } from "../../../Pages/Landing/modules/constants/landing.const";
import amarbank from '../../../Assets/images/amarbank-footer.svg'
import ojk from '../../../Assets/images/ojk-footer.svg'



const Footer = () => {
  return (
    <div className="footer">
      <div className="footer-container">
        <h6 className="footer-title">Senyumku Deposito</h6>

        <div className="footer-list-container">
          {FOOTER_DATA_LIST.map((item, idx) => (
            <div className="footer-list" key={idx}>
              <img src={item.img} alt="" />
              <a href="###" alt="">
                {item.text}
              </a>
            </div>
          ))}
        </div>
        
          <h6 className="footer-title">Alamat Kantor</h6>
          <div className="footer-address">
            Thamrin Residence Blok RA No. 07-08, Thamrin Boulevard, Tanah Abang,
            RT.3/RW.8, Kb. Melati, Kota Jakarta Pusat, Daerah Khusus Ibukota
            Jakarta 10220
          </div>

          <div className="footer-flex-container">
              <div className="footer-flex">
                <h6 className="footer-title">No. Telepon</h6>
                <div className="no-telepon">(021) 40002979</div>
              </div>

              <div className="footer-flex">
                <h6 className="footer-title">Jam Operasional</h6>
                <div className="no-telepon">24 Jam</div>
              </div>
          </div>
          
          <h6 className="footer-title">
                Senyumku Deposito adalah produk dari PT. Bank Amar 
                Indonesia yang Terdaftar dan Diawasi oleh
          </h6>

           <div className="footer-flex-image">
                <img src={amarbank} alt=""/>
                <img src={ojk} alt=""/>
           </div>

           <div className="footer-copyright">
               <strong>PT Bank Amar Indonesia Tbk.</strong>terdaftar dan diawasi oleh Otoritas Jasa Keuangan. 
               © 2020 PT Bank Amar Indonesia Tbk. Semua Hak Cipta Dilindungi.
           </div>

      </div>
    </div>
  );
};

export default Footer;
