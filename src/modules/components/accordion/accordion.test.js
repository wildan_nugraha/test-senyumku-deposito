import Accordion from './accordion'
import '@testing-library/jest-dom'
import {render,screen} from '@testing-library/react'

describe('Test accordion props', () => {
    const titles = "Apakah deposito dapat dicairkan sebelum jatuh tempo?"
    const desc = "Ketika deposito kamu telah cair/jatuh tempo, maka saldo dari deposito akan otomatis berpindah ke saldo Senyumku sebagai saldo aktif. Di saldo Senyumku, kamu dapat memilih untuk membuat deposito baru atau menarik saldo (transfer keluar ke bank lain)."
    
    it("Accordion props title", async ()=>{
      render(<Accordion title={titles} desc={desc} />);
      const element = screen.getByText("Apakah deposito dapat dicairkan sebelum jatuh tempo?")
       expect(element).toBeInTheDocument();
    })

    it("Accordion props description", async ()=>{
      render(<Accordion title={titles} desc={desc} />);
      const element = screen.getByText(desc)
       expect(element).toBeInTheDocument();
    })
});





