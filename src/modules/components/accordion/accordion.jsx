import React,{useState} from 'react'
import './Accordion.scss'
import chevron from '../../../Assets/images/chevron.svg'


const Accordion = ({title,desc}) => {

 
    
    const [isShow, setIsShow] = useState(false);

    const handleShowAccordion = () => {
        setIsShow(!isShow)
    }

  return (
    <div className='accordion' data-testid="accordion">
        <button className='accordion_btn' onClick={handleShowAccordion}>
            <span>{title}</span>
            <img src={chevron} alt='' className={isShow ? 'chevron-up' : 'chevron-down'}/>
        </button>
        <div className={isShow ? 'accordion_body-show' : 'accordion_body-hide'}>
           {desc}
        </div>
        
    </div>
  )
}

export default Accordion