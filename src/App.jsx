import React from "react";
import useLocalStorage from "./modules/services/useLocalStorage";
import { createContext } from "react";
import Header from "./modules/components/header/header";
import Landing from "./Pages/Landing/Landing";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Registration from "./Pages/Registration/registration";
import RegistrationVerify from "./Pages/Registration-verify/registration-verify";
import "@splidejs/splide/dist/css/splide.min.css";

export const globalState = createContext(null);

const App = () => {
  const [dataForm, setDataForm] = useLocalStorage("dataForm", null);
  const [dataForm2, setDataForm2] = useLocalStorage("dataForm2", null);
  
  return (
    <div className="App">
      <BrowserRouter>
        <globalState.Provider
          value={{ dataForm, setDataForm, dataForm2, setDataForm2 }}
        >
          <Header />

          <Switch>
            <Route exact path="/">
              <Landing />
            </Route>

            <Route path="/daftar">
              <Registration />
            </Route>

            <Route path="/verifikasi">
              <RegistrationVerify />
            </Route>
          </Switch>
        </globalState.Provider>
      </BrowserRouter>
    </div>
  );
};

export default App;
