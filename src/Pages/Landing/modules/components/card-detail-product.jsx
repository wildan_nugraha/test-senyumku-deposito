import React from "react";
import "../../../../Assets/styles/components/card-detail-product.scss";

const CardDetailProduct = () => {
  return (
    <div className="card">
      <div className="card_deposito-container">
        <h5>Deposito</h5>
        <p>
          Investasi rendah resiko dengan keuntungan yang lebih tinggi dari
          tabungan biasa
        </p>

        <div className="card_flex-container">
          <div className="card_deposito-flex">
              <svg fill="#009B9D" width="16" height="16" viewBox="0 0 16 16">
                <path d="M7.072 15.897L8 16C7.531 12.247 4.55 9.306.79 8.887L0 8.8l.017.138c.46 3.672 3.377 6.55 7.055 6.959zm8.911-6.959L16 8.8l-.79.087C11.45 9.306 8.47 12.247 8 16l.928-.103c3.678-.409 6.595-3.287 7.055-6.959zM12.85 2.8c-.552-.956-1.776-1.284-2.732-.732-.05.028-.088.065-.134.097C9.988 2.11 10 2.056 10 2c0-1.105-.895-2-2-2S6 .895 6 2c0 .056.012.11.017.165-.046-.032-.086-.069-.135-.097-.956-.552-2.18-.224-2.732.732s-.224 2.18.732 2.732c.05.028.101.045.15.068-.049.023-.1.04-.15.068-.956.552-1.284 1.776-.732 2.732.552.956 1.776 1.284 2.732.732.05-.028.09-.065.135-.097C6.012 9.09 6 9.144 6 9.2c0 1.105.895 2 2 2s2-.895 2-2c0-.056-.012-.11-.017-.165.046.032.085.069.135.097.956.552 2.18.224 2.732-.732s.224-2.18-.732-2.732c-.05-.028-.101-.045-.15-.068.05-.023.1-.04.15-.068.956-.552 1.284-1.776.732-2.732zM8 8C6.674 8 5.6 6.926 5.6 5.6c0-1.326 1.074-2.4 2.4-2.4 1.326 0 2.4 1.074 2.4 2.4C10.4 6.926 9.326 8 8 8z" transform="translate(-31 -1200) translate(17 1185) translate(14 7) translate(0 8)"></path>
              </svg>
              <p>Bunga tinggi hingga<span>9%</span>per tahun</p>
          </div>

          <div className="card_deposito-flex">
              <svg fill="#009B9D" width="16" height="20" viewBox="0 0 16 16">
                <path d="M15.2 0H.8C.358 0 0 .391 0 .875v12.25c0 .484.358.875.8.875h14.4c.442 0 .8-.391.8-.875V.875C16 .391 15.642 0 15.2 0zm-.8 9.625c-1.326 0-2.4 1.175-2.4 2.625H4c0-1.45-1.074-2.625-2.4-2.625v-5.25C2.926 4.375 4 3.2 4 1.75h8c0 1.45 1.074 2.625 2.4 2.625v5.25z" transform="translate(-44 -315) translate(21 167) translate(23 145) translate(0 3)"></path>
                <path d="M8 3.5C6.235 3.5 4.8 5.07 4.8 7s1.435 3.5 3.2 3.5c1.765 0 3.2-1.57 3.2-3.5S9.765 3.5 8 3.5zm0 5.25c-.882 0-1.6-.785-1.6-1.75S7.118 5.25 8 5.25c.882 0 1.6.785 1.6 1.75S8.882 8.75 8 8.75z" transform="translate(-44 -315) translate(21 167) translate(23 145) translate(0 3)"></path>
              </svg>
              <p>Minimal deposito hanya Rp<span>100.000</span></p>
          </div>

          <div className="card_deposito-flex">
              <svg fill="#009B9D" width="16" height="19" viewBox="0 0 16 16">
                <path d="M7 9L12 9 12 14 7 14z" transform="translate(-44 -343) translate(21 167) translate(23 176)"></path>
                <path d="M14.222 1.8h-1.778V0h-1.777v1.8H5.333V0H3.556v1.8H1.778C.798 1.8 0 2.607 0 3.6V16.2c0 .993.797 1.8 1.778 1.8h12.444c.98 0 1.778-.807 1.778-1.8V3.6c0-.993-.797-1.8-1.778-1.8zm.001 14.4H1.778V5.4h12.444l.001 10.8z" transform="translate(-44 -343) translate(21 167) translate(23 176)"></path>
              </svg>
              <p>Periode deposito dari 1 sampai 36 bulan</p>
          </div>
        </div>

        <div className="card_deposito-footer">
          <ul>
            <li>Bunga dihitung bulanan dan dapat ditarik <span><strong>pada akhir periode</strong></span></li>
          </ul>
        </div>

        

      </div>
    </div>
  );
};

export default CardDetailProduct;
