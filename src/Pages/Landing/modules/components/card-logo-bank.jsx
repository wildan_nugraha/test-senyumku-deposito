import React from "react";
import "../../../../Assets/styles/components/card-logo-bank.scss";
import { BANK_LOGO } from "../constants/landing.const";

const CardLogoBank = () => {
  return (
    <div className="logo_Wrapper">
      <div className="logoBankWrapper_title">Menerima Top Up via</div>

      <div className="logoBankWrapper_logo-wrapper">
        {BANK_LOGO.map((item, idx) => (
          <img src={item.img} alt="" key={idx} className="logo_bank" />
        ))}
      </div>
    </div>
  );
};

export default CardLogoBank;
