import React from "react";
import "../../../../Assets/styles/components/_jumbotron.scss";
import { Link } from "react-scroll";

const Jumbotron = () => {
  return (
    <div className="jumbotron">
      <h1 className="jumbotron_title">Buka deposito bisa dari rumah</h1>
      <p className="jumbotron_description">
        Cukup <b>daftar online</b> , nikmati deposito dengan <b>bunga hingga 9%</b> per tahun
        di Senyumku Deposito
      </p>

      <Link to="card_simulasi-deposito" alt="" smooth={true} duration={1000}>
        <button className="jumbotron_button">
          Coba simulasi deposito
        </button>
      </Link>
    </div>
  );
};

export default Jumbotron;
