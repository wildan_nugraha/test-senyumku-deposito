import React from "react";
import "../../../../Assets/styles/components/card-others-product.scss";
import logo from "../../../../Assets/images/senyumku-logo.svg";
import CarouselJSON from '../../../../Assets/json/carousel-json.json'
import Carousel from "../../../../modules/components/carousel/carousel";

const CardOthersProduct = () => {
  return (
    <div className="card_produk-container">
      <div className="card_produk-container-flex">
        <div className="card_produk card-1">
          <div className="card_produk-header">Produk</div>
          <div className="card_produk-body">
            <img src={logo} alt="" />
            <div className="card_produk-text blue">Deposito Bank Lain*</div>
            <div className="card_produk-text">Reksadana**</div>
          </div>
        </div>

        <Carousel data={CarouselJSON}/>
        
      </div>

      <div className="index_text-container">
        <i>
          *Sumber: Pusat Informasi Pasar Uang (PIPU) untuk kategori Bank BUKU IV
          (per 22 Sep 20)
        </i>
        <br />
        <i>
          **Sumber: www.pasardana.id untuk kategori 5 Reksadana Pasar Uang
          dengan bunga tertinggi dan dana kelolaan diatas 500M (per 22 Sep 20).
        </i>
      </div>
    </div>
  );
};

export default CardOthersProduct;
