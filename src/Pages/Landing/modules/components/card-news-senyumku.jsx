import React from "react";
import "../../../../Assets/styles/components/card-news-senyumku.scss";
import { SENYUMKU_NEWS } from "../constants/landing.const";
import { Divider } from "antd";

const CardNewsSenyumku = () => {
  return (
    <div className="card">
      {SENYUMKU_NEWS.map((item, idx) => (
        <a href={item.link} key={idx} target="_blank" rel="noreferrer" className="card_liputan-list">
          <div className="Card_list-liputan">
            <div className="card_liputan-text">
              <div>{item.text}</div>
              <div>{item.source}</div>
            </div>
            <img className="card_liputan-photo" src={item.img} alt="" />
          </div>
          {idx < SENYUMKU_NEWS.length-1 && <Divider />}
        </a>
      ))}
    </div>
  );
};

export default CardNewsSenyumku;
