import React from "react";
import "../../../../Assets/styles/components/card-why-senyumku.scss";
import { WHY_SENYUMKU } from "../constants/landing.const";
import { Divider } from "antd";

const CardWhySenyumku = () => {
  return (
    <div className="card">
      {WHY_SENYUMKU.map((item, idx) => (
        <div key={idx}>
          <div className="Card_list">
            <img className="card_list-image" src={item.img} alt="" />
            <div className="card_list-text">
              <div>{item.text1}</div>
              <div>{item.text2}</div>
            </div>
          </div>
          {idx < WHY_SENYUMKU.length-1 && <Divider />}
        </div>
      ))}
    </div>
  );
};

export default CardWhySenyumku;
