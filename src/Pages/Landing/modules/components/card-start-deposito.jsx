import React from "react";
import "../../../../Assets/styles/components/card-start-deposito.scss";
import { START_DEPOSITO } from "../constants/landing.const";


const CardStartDeposito = () => {
  return (
    <div className="card">
      {START_DEPOSITO.map((data) => (
        <div className="card_list" key={data.no}>
          <div className="card_list-number">{data.no}</div>
          <div className="card_list-text">
            {data.desc}
          </div>
        </div>
      ))}
    </div>
  );
};

export default CardStartDeposito;
