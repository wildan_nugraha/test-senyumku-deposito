import React from 'react'
import '../../../../Assets/styles/components/card-logo-transaction-security.scss'
import { SECURITY_TRANSACTION_LOGO } from '../constants/landing.const'

const cardLogoTransactionSecurity = () => {
  return (
    <div className='logo_Wrapper'>
        <div className="logoBankWrapper_title">Keamanan Transaksi</div>
       <div className='logoKeamanan_wrapper'>
           {SECURITY_TRANSACTION_LOGO.map((item,idx)=>(
               <img src={item.img} alt="" key={idx}/>
           ))}
       </div>
    </div>
  )
}

export default cardLogoTransactionSecurity;