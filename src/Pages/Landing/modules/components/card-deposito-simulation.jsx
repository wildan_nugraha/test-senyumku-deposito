import React, { useState,useEffect} from "react";
import "../../../../Assets/styles/components/card-deposito-simulation.scss";
import { DEPOSITO_SIMULATION } from "../constants/landing.const";
import CurrencyInput from "react-currency-input-field";
import { Divider, Radio } from "antd";
import { depositoCalculation, bankInterest } from "../services/depostio-calculation.hook";


const CardSimulasiDeposito = () => {
  const [inputValue, setInputValue] = useState(1000000);
  const [bankParam, setBankParam] = useState({
    interest: 0.07,
    period: 360
  });
  const [nominal, setNominal] = useState(0);
  
  useEffect(() => {
    setNominal(depositoCalculation(inputValue,bankParam));
  }, [inputValue,bankParam]);

  return (
    <div className="card">
      <div className="card_text-1">Nominal Deposito</div>
      <div className="card_input-currency">
        <div className="label">Rp</div>
        <CurrencyInput
          id="input-currency"
          name="input-currency"
          defaultValue={inputValue}
          decimalsLimit={2}
          groupSeparator="."
          decimalSeparator=","
          onValueChange={(value) => setInputValue(Number(value))}
        />
      </div>
      <div className="card_text-2">Minimal deposito Rp100.000</div>
      
      {(inputValue < 100000 || nominal === "NaN") && <div className="error_validation">Minimal nominal 100 ribu</div>}

      <Divider />
      <div className="card_text-3">Pilih Periode dan Bunga per tahun</div>

     <div id="ant-group-solid">
      <Radio.Group buttonStyle="solid" optionType="button" defaultValue={7}>
          {DEPOSITO_SIMULATION.map((item, idx) => (
            <Radio.Button value={item.bunga} 
              key={idx} 
              onChange={()=> setBankParam(bankInterest(item))}
              >
                {item.bulan} | {item.bunga}%
            </Radio.Button>
          ))}
        </Radio.Group>
     </div>

      <div className="card_text-4">Estimasi Saldo di Akhir Periode</div>
      <div className="card_text-5">*belum termasuk pajak</div>
      <div className="card_text-saldo">
        Rp {inputValue < 100000 || nominal === "NaN" ? 0 : nominal}.-
      </div>
      <Divider />
      <button 
        className="btn_Card-deposito" 
        disabled={inputValue < 100000?true:false}
        onClick={()=>alert("hallo")}>
         Mulai Deposito
      </button>
    </div>
  );
};

export default CardSimulasiDeposito;
