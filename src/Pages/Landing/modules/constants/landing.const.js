import news1 from "../../../../Assets/images/news-line-1.png";
import news2 from "../../../../Assets/images/news-line-2.png";
import news3 from "../../../../Assets/images/news-line-3.png";

import terjamin from "../../../../Assets/images/icon-terjamin.svg";
import bunga from "../../../../Assets/images/icon-bunga-tinggi.svg";
import cepat from "../../../../Assets/images/icon-cepat-mudah.svg";

import bca from "../../../../Assets/images/bca.svg";
import mandiri from "../../../../Assets/images/madiri.svg";
import bri from "../../../../Assets/images/bri.svg";
import bni from "../../../../Assets/images/bni.svg";
import ovo from "../../../../Assets/images/ovo.svg";
import visa from "../../../../Assets/images/visa.svg";
import master from "../../../../Assets/images/master.svg";
import jcb from "../../../../Assets/images/jcb.svg";

import vs from '../../../../Assets/images/verified-visa.svg'
import mc from '../../../../Assets/images/mc-logo.svg'
import jsc from '../../../../Assets/images/jsc-mc.png'

import arrow from "../../../../Assets/images/arrow.svg";

import logoAmar from '../../../../Assets/images/logo-amar.svg'
import ojk from '../../../../Assets/images/ojk-logo.svg'
import indBankAward from '../../../../Assets/images/indonesia-bank-award.png'

export const DEPOSITO_SIMULATION = [
    {
        bulan: "1 bulan",
        bunga: 5.75,
        periode : 30
      },
      {
        bulan: "3 bulan",
        bunga: 6,
        periode : 90
      },
      {
        bulan: "6 bulan",
        bunga: 6.25,
        periode : 180
      },
      {
        bulan: "12 bulan",
        bunga: 7,
        periode : 360
      },
      {
        bulan: "18 bulan",
        bunga: 7.5,
        periode : 540
      },
      {
        bulan: "24 bulan",
        bunga: 8,
        periode : 720
      },
      {
        bulan: "36 bulan",
        bunga: 9,
        periode : 1080
      },
]

export const SENYUMKU_NEWS = [
    {
        img: news1,
        text: "Bank Amar Catatkan Laba 25,6 Miliar Pada Kuartal 3 2020",
        source: "Liputan 6",
        link: "https://www.liputan6.com/bisnis/read/4398031/amar-bank-bukukan-laba-bersih-rp-256-miliar-hingga-september-2020"
      },
      {
        img: news2,
        text: "Aset Amar Bank Naik 13 Kali Lipat Capai Rp 3,1 Triliun",
        source: "Tribun News",
        link:"https://www.tribunnews.com/bisnis/2020/08/26/aset-amar-bank-naik-13-kali-lipat-capai-rp-31-triliun"
      },
      {
        img: news3,
        text: "Gandeng Google, Amar Bank Perkenalkan Senyumku",
        source: "Bisnis.com",
        link:"https://finansial.bisnis.com/read/20200706/563/1262147/gandeng-google-amar-bank-perkenalkan-senyumku"
      },
]

export const START_DEPOSITO = [
    {
        no: 1,
        desc: "Daftar menggunakan nomor handphone dan email",
      },
      {
        no: 2,
        desc: "Setelah daftar dan masuk ke akun, lengkapi data tambahan untuk dapat melakukan Top Up",
      },
      {
        no: 3,
        desc: "Pilih metode pembayaran dan ikuti instruksi Top Up",
      },
      {
        no: 4,
        desc: "Mulai deposito dan pantau pertumbuhannya secara rutin",
      },
]

export const WHY_SENYUMKU = [
    {
        img: terjamin,
        text1: "Investasi pasti UNTUNG",
        text2: "Saldo kamu pasti tumbuh setiap bulan",
      },
      {
        img: bunga,
        text1: "Uangmu dijamin AMAN",
        text2: "Hanya bisa tarik saldo ke rekening atas nama kamu",
      },
      {
        img: cepat,
        text1: "Proses daftar CEPAT & MUDAH",
        text2: "Daftar dan lengkapi data secara online (hanya 5 menit)",
      },
]

export const BANK_LOGO = [
    {
        img: bca,
      },
      {
        img: mandiri,
      },
      {
        img: bri,
      },
      {
        img: bni,
      },
      {
        img: ovo,
      },
      {
        img: visa,
      },
      {
        img: master,
      },
      {
        img: jcb,
      },
]

export const SECURITY_TRANSACTION_LOGO = [
    {
        img:vs
    },
    {
        img:mc
    },
    {
        img:jsc
    }
]

export const FOOTER_DATA_LIST = [
    {
        text: "Tentang Kami",
        img: arrow,
      },
      {
        text: "Kontak Kami",
        img: arrow,
      },
      {
        text: "Kebijakan Privasi",
        img: arrow,
      },
      {
        text: "Syarat & Ketentuan",
        img: arrow,
      },
]

export const LANDING_LOGO = [
    {
        img: logoAmar
    },
    {
        img: ojk
    },
    {
        img: indBankAward
    }
]