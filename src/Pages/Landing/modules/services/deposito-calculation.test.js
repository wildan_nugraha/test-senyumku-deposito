import {depositoCalculation} from './depostio-calculation.hook'

const array = [
    {
        interest: 0.0575,
        period: 30,
        result: "1.004.726"
    },
    {
        interest: 0.06,
        period: 90,
        result : "1.014.794"
    },
]

describe('Deposito calculation test', () => {
    const inputValue = 1000000
    array.forEach(element => {
        it('Return valid calculation each month', ()=>{
            const result = element.result
            expect(depositoCalculation(inputValue,element)).toBe(result)
        })
    });
});




