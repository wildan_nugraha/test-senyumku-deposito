import toRupiah from "@develoka/angka-rupiah-js";

export const bankInterest = (value) =>{
    const values = {}
    values.interest = value.bunga / 100
    values.period = value.periode
    return values
}

export const depositoCalculation = (inputValue,bankParam) =>{
    const INVEST_RESULT =  inputValue * bankParam.interest * (bankParam.period/365) + inputValue
    return toRupiah(INVEST_RESULT, { dot: ".", floatingPoint: 0,symbol:null });
}