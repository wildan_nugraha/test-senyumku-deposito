import React from 'react'
import './Landing.scss'
import Jumbotron from './modules/components/jumbotron'
import { LANDING_LOGO } from './modules/constants/landing.const'
import chevron from "../../Assets/images/chevron.svg"
import CardDetailProduct from './modules/components/card-detail-product'
import CardDepositoSimulation from './modules/components/card-deposito-simulation'
import CardOthersProduct from './modules/components/card-others-product'
import CardWhySenyumku from './modules/components/card-why-senyumku'
import CardNewsSenyumku from './modules/components/card-news-senyumku'
import CardStartDeposito from './modules/components/card-start-deposito'
import CardLogoBank from './modules/components/card-logo-bank'
import CardLogoTransactionSecurity from './modules/components/card-logo-transaction-security'
import Accordion from '../../modules/components/accordion/accordion'
import FaqJSON from "../../Assets/json/faq-json.json"
import Footer from '../../modules/components/footer/footer'
import { BackTop } from 'antd';


const Landing = () => {

  return (
    <div className='landing_page'>
     
      <Jumbotron/>

      <div className="landing_page-static">
      
        <div className="landing_page-logo-container">
          <div className="landing_page-logo">
            {LANDING_LOGO.map((item,idx)=>(
              <img src={item.img} alt="" className='img-png' key={idx}/>            
            ))}
          </div>
        </div>

        <div className="landing_page-dana-container">
            <div className='dana-text'>Total Dana yang telah dikelola Bank</div>
            <div className="flex">
              <div className="arrow-up"></div>
              <div className="dana-jumlah">
                Rp. <span><strong>2.24 Triliun</strong></span>
              </div>
            </div>
            <div className="dana-update">
                Terakhir di update : Juli 2021
            </div>
        </div>

        <div className="landing_page-arrow">
          <img src={chevron} alt="" className='slide-bottom '/>
          <img src={chevron} alt="" className='slide-bottom slide-bottom1'/>
        </div>
      </div>

      <div className="landing_page-card-container" >
          <div className='card-container'>
            <h6 className='landing_page-subtitle'>Detail Produk</h6>
            <CardDetailProduct/>
          </div>

          <div className='card-container' >
            <h6 className='landing_page-subtitle' id="card_simulasi-deposito">Simulasi Deposito</h6>
            <CardDepositoSimulation/>
          </div>
          
          <div className='card-container'>
            <h6 className='landing_page-subtitle'>Senyumku Deposito vs Produk Lain</h6>
            <CardOthersProduct/>
          </div>

          <div className='card-container'>
            <h6 className='landing_page-subtitle'>Kenapa Senyumku Deposito?</h6>
            <CardWhySenyumku/>
          </div>

          <div className='card-container'>
            <h6 className='landing_page-subtitle'>Liputan Senyumku</h6>
            <CardNewsSenyumku/>
          </div>
          
          <div className='card-container'>
            <h6 className='landing_page-subtitle'>Cara Memulai Deposito</h6>
            <CardStartDeposito/>
          </div>
          
      </div>

      <div className='landing_page-logo-container'>
        <CardLogoBank/>
        <CardLogoTransactionSecurity/>
      </div>

      <div className='landing_page-accordion-container'>
        <div className='wrapper'>
          <h6>FAQ</h6>
          {FaqJSON.map((item,idx)=>(
            <Accordion title={item.title} desc={item.desc} key={idx}/>
          ))}
        </div>
      </div>

      <Footer/>

      <BackTop/>
        
      
    </div>
  )
}

export default Landing