import React,{useContext} from 'react'
import './registration-verify.scss'
import { globalState } from '../../App'
import StepComponent from '../../modules/components/steps/steps'
import { Button, Divider } from 'antd'
import {tooltipMessage} from '../../modules/services/tooltip-message.hook'

const VerifikasiAkun = () => {
    const {dataForm} = useContext(globalState)
    const {dataForm2} = useContext(globalState)

    const {ktp,email,noHandphone,password,passwordConfirm,otp} = dataForm 
    const {genderUser,tglLahir} = dataForm2 
    
    const OTP_SPLIT = [...otp.toString()]

  return (
    <div className='verifikasiAkun'>
        <StepComponent stepNum={1}/>
        <Divider/>
        <div className='dataForm'>
            <div className='dataForm_title'>Verifikasi Data Anda</div>
            <div className='dataForm_text'>Kode OTP anda :</div>
            <div className='otp_number-card-wrapper'>
                {OTP_SPLIT.map((item,idx)=>(
                    <div className='otp_number-card' key={idx}>{item}</div>
                ))}
            </div>
           <div className='dataForm_box'>
                <div>No. KTP : {ktp}</div>
                <div>Tanggal lahir : {tglLahir}</div>
                <div>Gender : {genderUser}</div>
                <div>Email : {email}</div>
                <div>No handphone : {noHandphone}</div>
                <div>Password : {password}</div>
                <div>Konfirmasi password : {passwordConfirm}</div>
                <Divider/>
                <div className='btn-wrapper'>
                    <Button type="primary" block
                    onClick={()=>tooltipMessage("Berhasil membuat akun baru!")}>Data sudah benar</Button>
                </div>
           </div>
        </div>
    </div>
  )
}

export default VerifikasiAkun