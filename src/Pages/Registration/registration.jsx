import React from 'react'
import "./registration.scss"
import { Divider } from 'antd'
import CardFormInfo from './modules/components/card-form-info'
import StepComponent from '../../modules/components/steps/steps'
import FormRegistration from './modules/components/form-registration'


const Registration = () => {
  return (
    <div className='registration'>
        <CardFormInfo/>
        <StepComponent stepNum={0}/>
        <Divider/>
        <FormRegistration/>   
    </div>
    
  )
}

export default Registration;