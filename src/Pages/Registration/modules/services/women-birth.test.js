import {womenBirth} from './women-birth.hook'
describe('Women birth date test', () => {
    const input = "3201015911990017"
    it('Return women birth date valid', ()=>{
        expect(womenBirth(input)).toBe("191199")
    })
});
