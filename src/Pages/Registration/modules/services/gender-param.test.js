import { genderParam } from "./gender-param.hook";

describe('Gender select test', () => {
  
    it("Return gender Laki-Laki", ()=>{
        expect(genderParam("3201010911990017")).toBe("Laki-Laki")
    })

    it("Return gender Perempuan", ()=>{
        expect(genderParam("3201015911990017")).toBe("Perempuan")
    })
});
