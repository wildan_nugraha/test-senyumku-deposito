import {
  MIN_16_DIGIT_REGEX,
  EMAIL_FORMAT_REGEX,
  MIN_12_DIGIT_REGEX,
  PASSWORD_FORMAT_REGEX,
} from "../../../../modules/constants/regex.const";

import validationMessage from "../../../../Assets/json/validation-message.json";

const {
  FIELD_KTPNUMBER_EMPTY,
  FIELD_KTPNUMBER_EXACT_LENGTH,
  FIELD_EMAIL_EMPTY,
  FIELD_EMAIL_WRONG_FORMAT,
  FIELD_PHONENUMBER_EMPTY,
  FIELD_PHONENUMBER_BETWEEN_LENGTH,
  FIELD_PASSWORD_EMPTY,
  FIELD_PASSWORD_WRONG_FORMAT,
  FIELD_CONFIRM_PASSWORD_EMPTY,
  FIELD_CONFIRM_PASSWORD_NOT_MATCH,
} = validationMessage;

export const registrationValidator = (value) => {
  const errors = {};

  if (!value.ktp) {
    errors.ktp = FIELD_KTPNUMBER_EMPTY;
  } else if (!MIN_16_DIGIT_REGEX.test(value.ktp)) {
    errors.ktp = FIELD_KTPNUMBER_EXACT_LENGTH;
  }

  if (!value.email) {
    errors.email = FIELD_EMAIL_EMPTY;
  } else if (!EMAIL_FORMAT_REGEX.test(value.email)) {
    errors.email = FIELD_EMAIL_WRONG_FORMAT;
  }

  if (!value.noHandphone) {
    errors.noHandphone = FIELD_PHONENUMBER_EMPTY;
  } else if (!MIN_12_DIGIT_REGEX.test(value.noHandphone)) {
    errors.noHandphone = FIELD_PHONENUMBER_BETWEEN_LENGTH;
  }

  if (!value.password) {
    errors.password = FIELD_PASSWORD_EMPTY;
  } else if (!PASSWORD_FORMAT_REGEX.test(value.password)) {
    errors.password = FIELD_PASSWORD_WRONG_FORMAT;
  }

  if (!value.passwordConfirm) {
    errors.passwordConfirm = FIELD_CONFIRM_PASSWORD_EMPTY;
  } else if (value.passwordConfirm !== value.password) {
    errors.passwordConfirm = FIELD_CONFIRM_PASSWORD_NOT_MATCH;
  }

  return errors;
};
