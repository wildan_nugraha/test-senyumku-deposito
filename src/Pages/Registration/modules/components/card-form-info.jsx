import React from 'react'
import '../../../../Assets/styles/components/card-form-info.scss'
import info from '../../../../Assets/images/info.svg'

const CardFormInfo = () => {
  return (
    <div className='cardInfo'>
        <img src={info} alt=''  className='cardInfo-icon'/>
        <div className='cardInfo-text'>
            Kerahasiaan informasi kamu akan kami jaga
        </div>
    </div>
  )
}

export default CardFormInfo;