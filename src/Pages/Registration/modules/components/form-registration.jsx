import React, { useState, useContext,useEffect} from "react";
import { globalState } from "../../../../App";
import "../../../../Assets/styles/components/form-registration.scss";
import { Formik } from "formik";
import { Input, DatePicker, Form, FormItem, Select } from "formik-antd";
import { message,Row,Col } from "antd";
import moment from "moment";
import { useHistory } from "react-router-dom";
import {registrationValidator} from "../validators/registration.validator";
import { genderParam } from "../services/gender-param.hook";
import { womenBirth} from "../services/women-birth.hook";

const { Option } = Select;

const FormRegistration = () => {
  const history = useHistory()
  
  const [inputKTP, setInputKTP] = useState(''); 
  const [paramWomenBirth, setParamWomenBirth] = useState(null);
  const [gender, setGender] = useState(null);
  const { setDataForm } = useContext(globalState);
  const { setDataForm2 } = useContext(globalState);
 
  useEffect(() => {
    setDataForm2({genderUser:gender, birth:""})
    setGender(genderParam(inputKTP))
    setParamWomenBirth(womenBirth(inputKTP)) 
  }, [inputKTP, gender]);


  return (
    <div className="formDaftar">
      <Formik
        initialValues={{
          ktp:inputKTP,
          email: "",
          noHandphone: "",
          password: "",
          passwordConfirm: "",
          otp: Math.floor(1000 + Math.random() * 9000),
        }}

        onSubmit={(values) => {
          message.loading({ content: "Loading...", key: "updatable" });
          setTimeout(() => {
            setDataForm(values);
            history.push({ pathname: "/verifikasi" });
            message.success({
              content: "Success!!",
              key: "updatable",
              duration: 2,
            });
          }, 1000);
        }}

        validate={(values) => {
          return registrationValidator(values)
        }}
      >
        {(props) => (
          <Form layout="vertical" size="large" onSubmit={props.handleSubmit}>
            <FormItem name="ktp" label="No. KTP" required={true}>
              <Input name="ktp"
                value={inputKTP}
                onChange={({target})=> setInputKTP(target.value)}/>
            </FormItem>

            <Row>
              <Col span={12}>
                  <FormItem name="tglLahir" label="Tanggal Lahir" required={true}>
                  <DatePicker
                    name="tglLahir"
                    value={inputKTP[6] > 3 ? moment(paramWomenBirth,"DD/MM/YYYY") : moment(inputKTP.substring(6,12),"DD/MM/YYYY")}
                    format={"DD/MM/YYYY"}
                    disabled={inputKTP.length < 16 ? true : false}
                    onClick={(e)=>setDataForm2({genderUser:gender, tglLahir:e.target.value})}
                   />
                  </FormItem>
              </Col>
              <Col span={12}>
                  <FormItem name="gender" label="Gender" required={true}>
                  <Select name="gender" style={{ width: 170 }} 
                    value={gender}
                    disabled={inputKTP.length < 16 ? true : false}
                    >
                    <Option value="Laki-Laki">Laki-Laki</Option>
                    <Option value="Perempuan">Perempuan</Option>
                  </Select>
                </FormItem>
              </Col>
            </Row>
            

            <FormItem 
              name="email" 
              label="Email" 
              required={true}>
              <Input name="email" />
            </FormItem>

            <FormItem
              name="noHandphone"
              label="Nomor Handphone"
              required={true}
            >
              <Input name="noHandphone" />
              <div className="note">
                Kode OTP akan dikirimkan ke nomor diatas
              </div>
            </FormItem>

            <FormItem 
              name="password" 
              label="Password" 
              required={true}>
              <Input.Password name="password" />
              <div className="note">
                Min. 8 karakter, terdiri dari huruf besar, huruf kecil, dan
                angka
              </div>
            </FormItem>

            <FormItem
              name="passwordConfirm"
              label="Konfirmasi Password"
              required={true}
            >
              <Input.Password name="passwordConfirm" />
            </FormItem>

            <div className="btn-wrapper">
              <button type="submit" className="btn-submit">
                Daftar
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default FormRegistration;
