import React from "react";
import ReactDOM from "react-dom";
import "../node_modules/normalize.css/normalize.css";
import App from "./App";
import "./Assets/styles/index.css";
import "./Assets/styles/Card.scss"
import "antd/dist/antd.css";

ReactDOM.render(<App />, document.getElementById("root"));
